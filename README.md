# Altcoin Facilitator
![HTML5](https://gitlab.com/Scotshield/Carteiras/raw/master/img/sources/html5.png)  ![CSS3](https://gitlab.com/Scotshield/Carteiras/raw/master/img/sources/css3.png)  ![JavaScript](https://gitlab.com/Scotshield/Carteiras/raw/master/img/sources/javascript.png)  ![Bootstrap](https://gitlab.com/Scotshield/Carteiras/raw/master/img/sources/bootstrap.png)

O Altcoin Facilitator é uma extensão criada para o Google Chrome que tem como propósito acompanhar o mercado de Altcoins; possibilitar a conversão de altcoins, salvar endereços de carteira, salvar links, acompanhar moedas específicas e receber notificações sobre elas. Criado por [Naun Lemos Belo](https://gitlab.com/Scotshield).

## Preview

![Altcoin Facilidator](https://gitlab.com/Scotshield/Carteiras/raw/master/img/presentation/altcoin-home.png)

## Download and Installation

O download pode ser feito pela Chrome Web Store:
* [Download da última versão](https://chrome.google.com/webstore/detail/altcoin-facilitator/ggbdejbelbpmhephlcodpeblgbbcpjgo?hl=pt-BR)

## Uso

### Apresentação

![Apresentacao Tela Inicial](https://gitlab.com/Scotshield/Carteiras/raw/master/img/presentation/apresentacao_home.png)

Ao abrir a extensão será apresentado automaticamente informações das 15 maiores Altcoins do mercado (o ranking é feito com base no capital de mercado da moeda e não em seu valor unitário). 

As 3 maiores moedas são apresentadas com informações mais detalhas que as demais, como percentual de oscilação na ultima hora, nas últimas 24 horas e na última semana. Além disso, essas moedas também apresentam o valor da criptomoeda em dólar e uma moeda secundária, como `real brasileiro`, `rublo`, `euro` e `peso argentino`. As demais moedas da lista são apresentadas de forma simples, com nome, valor em dólar e percentual de oscilação na última hora.

Ainda na página inicial é possível realizar a pesquisa de moedas pelo seu nome. Dessa forma, é possível acompanhar a evolução do mercado de criptomoedas que não estão entre as 15 maiores. A pesquisa retorna dados semelhantes aos apresentados nas 3 maiores altcoins do mercado.

Obs.: A quantidade de moedas apresentadas pode ser modificada na página de configuração, bem como a `moeda secundária`, que por default é o Euro.

### Conversão

![Apresentacao Conversor](https://gitlab.com/Scotshield/Carteiras/raw/master/img/presentation/apresentacao_conversor.png)

A conversão entre moedas é possível entre as `2 mil` maiores altcoins em valor de mercado capitalizado. Desse modo, é possível converter `Bitcoin` em moedas pequenas que valem, por enquanto, praticamente nada. Também possível converter qualquer dessas moedas em `Dólar Americano` ou em `Real brasileiro`.

### Salvar Carteiras e Faucethub

![Apresentacao Salvar Carteira](https://gitlab.com/Scotshield/Carteiras/raw/master/img/presentation/apresentacao_wallets.png)

Visando dar facilidade ao acesso do endereço de sua carteira, foi criado a funcionalidade de salvar e excluir endereços de carteiras, seja de exchanges, seja uma carteira baixada em sua máquina (wallet pessoal). 

![Apresentacao FaucetHub Wallets](https://gitlab.com/Scotshield/Carteiras/raw/master/img/presentation/apresentacao_faucethub.png)

Além disso, para facilitar acesso à faucet àqueles que gostam de resolvê-las em trocar de altcoins, foi criada uma área para salvar os endereços de carteiras da FaucetHub.

### Listas

Listas para acesso fáceis, como lista com links das principais exchanges do mundo.

### Configuração

![Apresentacao Configuração](https://gitlab.com/Scotshield/Carteiras/raw/master/img/presentation/apresentacao_config.png)

Na tela de configuração é possível alterar a quantidade de moedas que são apresentadas na tela inicial (até o limite de 30). Também é possível alterar a moeda secundária. Por padrão o Euro é a moeda secundária, assim, as informações são dadas em Dólar e Euro. As moedas secundárias possível são: `real`, `euro`, `rublo` e `peso argentino`. 

### Dados

Os dados usados são abertos. Eles são fornecidos pela [coinmarketcap](http://www.coinmarketcap.com) através de sua API. 

### Sobre e Doação

Para ajudar o criador da extensão há a disponibilização de endereços de carteira Bitcoin, Bitcoin Cash e Ethereum na aba `SOBRE`, bem como a possibilidade de doação através do PagSeguro. 

## Idioma

A extensão foi traduzida para 3 idiomas:
* Inglês (`default`)
* Português
* Espanhol
O idioma apresentado é o mesmo configurado para o Google Chrome.

## Copyright and License

Copyright 2018 - Naun Lemos Belo. 
