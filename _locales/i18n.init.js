/**
 * data-i18n-attr   = indica qual atributo terá seu valor substituido pela mensagem traduzida. Quando o valor for 'innerHtml', o valor a ser substituído não é de um atributo, mas o que está no html.
 * data-i18n-label  = indica o nome da variável guardada no json com a mensagem traduzida.
 * Dessa forma
 * _locales/en/messages.json
 * >> {
 * >>   "hello": "Hello World"     
 * >> }
 *  *
 * index.html
 * >> <a href="#" data-i18n-attr="innerHtml" data-i18n-label="hello">A</a>
 * FICA:
 * >> <a href="#" data-i18n-attr="innerHtml" data-i18n-label="hello">Hello World</a>
**/

function i18n(){
    var i18n_labels = document.querySelectorAll('[data-i18n-attr]');
    for(var i=0;i<i18n_labels.length; i++){
        var message = chrome.i18n.getMessage(i18n_labels[i].getAttribute("data-i18n-label"));
        var attr = i18n_labels[i].getAttribute("data-i18n-attr");
        if(attr.toUpperCase() == 'innerHtml'.toUpperCase()){
            i18n_labels[i].innerHTML = message;
        }else{
            i18n_labels[i].setAttribute(attr, message);
        }
    }
}

i18n();
