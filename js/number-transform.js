function number_transform(numero){
	if(numero != null){
		var num_leng = numero.replace(/\..*/, '').length;
		if(num_leng > 9){
			return (numero / 1000000000).toFixed(3) + ' bi';
		}else if(num_leng > 6){
			return (numero / 1000000).toFixed(3) + ' mi';
        }else if(num_leng > 3){
			return (numero / 1000).toFixed(3) + ' mil';
        } else{
			return parseFloat(numero).toFixed(2);        	
        }
	}	
}