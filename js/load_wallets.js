      var keys_local_storage = [];

      //percorrendo as keys do localStorage
      for (var key in localStorage){
          if(key.match(/wallet_pessoal_exchange_coin_/)){
            keys_local_storage.push(key);
          }
      }
      
      $(document).ready(function() { 
        var html = "";
        load_wallets();

        function load_wallets(){
          for(var i=0; i<keys_local_storage.length; i++){
            var key = keys_local_storage[i].replace('wallet_pessoal_exchange_coin_', '');

            html += ''+
                  '<div class="coin-item">'+
                    '<div class="delete-coin-item" data-refer-id="' + key + '"><i class="fas fa-times-circle"></i></div>'+
                    '<div class="row">'+
                      '<div class="col-4">'+
                        '<p class="text-right">' + chrome.i18n.getMessage('wallet_saved_label_coin') + '</p>'+
                      '</div>'+
                      '<div class="col-7">'+
                        '<span class="coin-name data-wallet">' + localStorage.getItem('wallet_pessoal_exchange_coin_' + key) + '</span>'+
                      '</div>'+
                    '</div>'+
                    '<div class="row">'+
                      '<div class="col-4">'+
                        '<p class="text-right">' + chrome.i18n.getMessage('wallet_saved_label_wallet') + '</p>'+
                      '</div>'+
                      '<div class="col-8">'+
                        '<span class="wallet-address data-wallet">' + localStorage.getItem('wallet_pessoal_exchange_address_' + key) + '</span>'+
                      '</div>'+
                    '</div>'+
                    '<div class="row">'+
                      '<div class="col-4">'+
                        '<p class="text-right">' + chrome.i18n.getMessage('wallet_saved_label_exchange') + '</p>'+
                      '</div>'+
                      '<div class="col-8">'+
                        '<span class="exchange-name data-wallet">' + localStorage.getItem('wallet_pessoal_exchange_name_' + key) + '</span>'+
                      '</div>'+
                    '</div>'+
                  '</div>';
          }
          document.querySelector("#list-wallet").innerHTML = html; 
        }

        //deletando um item
        $(".delete-coin-item").on('click', function(){
          try{
            var key = $(this).attr("data-refer-id");
            
            localStorage.removeItem('wallet_pessoal_exchange_coin_' + key);
            localStorage.removeItem('wallet_pessoal_exchange_address_' + key);
            localStorage.removeItem('wallet_pessoal_exchange_name_' + key);

            $(this).parent().hide('slow', function(){ 
              $(this).remove(); 
            });

            flashMsg('success', chrome.i18n.getMessage('delete_success_msg')); //chrome.i18n
          }catch(e){
            flashMsg('danger', chrome.i18n.getMessage('delete_error_msg')); //chrome.i18n 
          }
        });

        function flashMsg(status, msg){
          $("#flash_msg").addClass('alert-' + status);
          $("#flash_msg").html(msg);
          $("#flash_msg").fadeIn("slow");
          setInterval(function(){
            $("#flash_msg").fadeOut("slow", function(){
              $("#flash_msg").removeClass('alert-' + status);           
            });
          },3000);
        }

      });