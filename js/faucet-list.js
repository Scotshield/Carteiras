
/*
      var json_faucethub_wallet_coins = '['+
                    '{'+
                       '"title_faucet": "Bitcoin Cash - 60 Satoshis",'+
                       '"time_faucet": "10 min",'+
                       '"faucet_hub": "Faucethub",'+
                       '"faucet_coins": ["BCH"],'+
                       '"faucet_desc": "Ganhe 60 satoshis de Bitcoin Cash <i>(ao todo)</i> a cada 10 minutos. Cada um dos sites abaixo dá 20 satoshis por <code>claim</code> direto na FaucetHub.",'+
                       '"faucet_name": ["One Way", "One Way", "tres tres", "quatro quatro", "cinco cinco"],'+
                       '"faucet_link": ["https://onewayfaucet.us/?r=1DFEby9e4tjKHSuPhLfWpEMn2BxZnTmrtp", "https://onewayfaucet.us/?r=1DFEby9e4tjKHSuPhLfWpEMn2BxZnTmrtp", "https://onewayfaucet.us/?r=1DFEby9e4tjKHSuPhLfWpEMn2BxZnTmrtp", "https://onewayfaucet.us/?r=1DFEby9e4tjKHSuPhLfWpEMn2BxZnTmrtp", "https://onewayfaucet.us/?r=1DFEby9e4tjKHSuPhLfWpEMn2BxZnTmrtp"]'+
                      '},'+
                    '{'+
                       '"title_faucet": "Instant Litecoin",'+
                       '"time_faucet": "0 min",'+
                       '"faucet_hub": "Direto",'+
                       '"faucet_coins": ["LTC"],'+
                       '"faucet_desc": "A instant-ltc acumula na conta até atingir 250.000 litoshis. Alcançando essa quantidade o pagamento é automático. Os litoshis vão acumulando com o tempo. Dessa forma, se você fizer o claim de 24h em 24h, você estará ganhando por volta de 3000 litoshis a cada claim.",'+
                       '"faucet_name": ["Instant LTC"],'+
                       '"faucet_link": ["https://www.instant-ltc.eu/en/ref-b9ZoiecQti"]'+
                      '}'+
                  ']';
      var faucets = JSON.parse(json_faucethub_wallet_coins);
*/

      $.ajax({
        type: "GET",
        url: "https://api.jsonbin.io/b/5aa284917417a517644f1a5a/latest",
        dataType: 'json',
        headers: {
          'secret-key': '$2a$10$EgI8bJXehacueI9HO2yP9.Y8.iNd6meH4B7dYx/5aAzjJkxDKnIWq'
        },
        success: function(data){
          load_json(data);
        },
        error: function(err){
          console.log(err.responseJSON);
        } 
      });


      function load_json(faucets){

      var html = '';
      for(var i =0; i<faucets.length; i++){
        html += '<div class="card margin-20">'+
                  '<div class="card-header" id="heading' + i + '">'+
                    '<h5 class="mb-0">'+
                      '<button class="btn btn-accordion ' + ((i==0) ? '' : 'collapsed') + '" data-toggle="collapse" data-target="#collapse' + i + '" aria-expanded="' + ((i==0) ? 'true' : 'false') + '" aria-controls="collapseOne">'+
                        '<span class="text-center">'+ faucets[i].title_faucet + '<br>'+
                          '<span class="badge badge-info">' + faucets[i].time_faucet + '</span>'+ 
                          '<span class="badge badge-primary">' + faucets[i].faucet_hub + '</span>';
                          for(var x=0; x<faucets[i].faucet_coins.length; x++){
                            html += '<span class="badge badge-warning">' + faucets[i].faucet_coins[x] + '</span>';
                          }
                          html+='</span>'+
                      '</button>'+
                    '</h5>'+
                  '</div>'+
                  '<div id="collapse' + i + '" class="collapse ' + ((i==0) ? 'show' : '') + '" aria-labelledby="heading' + i + '" data-parent="#accordion">'+
                    '<div class="card-body">'+ faucets[i].faucet_desc + 
                      '<div class="text-center">'+
                        '<div class="list-group faucet_list">'+
                          '<a href="#" class="list-group-item list-group-item-action active"> Links </a>';
                          for(var y=0; y<faucets[i].faucet_name.length; y++){
                            html += '<a href="' + faucets[i].faucet_link[y] + '" target="_blank" class="list-group-item list-group-item-action">'+ faucets[i].faucet_name[y] + '</a>';
                          }
                        html += '</div>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                '</div>';
      }
      document.querySelector("#accordion").innerHTML = html; 
}