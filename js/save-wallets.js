      $(document).ready(function() { 
        $('#check_orig_wallet').change(function() {
          if(!$(this).prop('checked')){
            $("#input_wallet_exchange").val(chrome.i18n.getMessage('save_wallet_type_owner')); //chrome.i18n
            $("#input_wallet_exchange").attr("disabled", "");
          }else{
            $("#input_wallet_exchange").val("");
            $("#input_wallet_exchange").removeAttr("disabled");
          }
        });

        $("#salvar").on('click', function(){
          var coin = $('#input_wallet_coin_name').val();
          var address = $('#input_wallet_address').val();
          var exchange = $("#input_wallet_exchange").val();
          var random = getRandomInt(1, 1000);

          if((coin != '') && (address != '') &&(exchange != '')){
            try{
              localStorage.setItem('wallet_pessoal_exchange_coin_' + coin + '_' +  random, coin); 
              localStorage.setItem('wallet_pessoal_exchange_address_' + coin + '_' + random, address);  
              localStorage.setItem('wallet_pessoal_exchange_name_' + coin + '_' + random, exchange);
              flashMsg('success', chrome.i18n.getMessage('save_success_msg')); //chrome.i18n
              
              //limpando o formulário
              $('#input_wallet_coin_name').val("");
              $('#input_wallet_address').val("");
              $("#input_wallet_exchange").val("");

            }catch(e){
              flashMsg('danger', chrome.i18n.getMessage('save_error_msg')); //chrome.i18n
            }
          }else{
            flashMsg('warning', chrome.i18n.getMessage('save_warning_msg')); //chrome.i18n
          }     
        });

        /* FUNÇÃO
         * Função que gera um número inteiro randômico dentro de um intervalo 
         */
        function getRandomInt(min, max) {
          min = Math.ceil(min);
          max = Math.floor(max);
          return Math.floor(Math.random() * (max - min)) + min;
        }

        function flashMsg(status, msg){
          $("#flash_msg").addClass('alert-' + status);
          $("#flash_msg").html(msg);
          $("#flash_msg").fadeIn("slow");
          setInterval(function(){
            $("#flash_msg").fadeOut("slow", function(){
              $("#flash_msg").removeClass('alert-' + status);           
            });
          },3000);
        }
      });
