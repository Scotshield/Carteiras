$( document ).ready(function() {

	var inputs = document.querySelectorAll('[data-clipboard-target]');

	for (i = 0; i < inputs.length; i++) {
	  (function(i) {
	    inputs[i].addEventListener('click', function() {
		  var copyTextarea = document.querySelector(inputs[i].getAttribute("data-clipboard-target")).getAttribute("value");
	  	  console.log(copyTextarea);

		  try {
		    var successful = copyToClipboard(copyTextarea);
		    $.notify("Endereço copiado",  { position:"left top", className: "success" });
		  } catch (err) {
			$.notify("Erro ao copiar",  { position:"left top", className: "error" });
		  }
	    });
	  })(i);
	}

	function copyToClipboard(text) {
	    if (window.clipboardData && window.clipboardData.setData) {
	        // IE specific code path to prevent textarea being shown while dialog is visible.
	        return clipboardData.setData("Text", text); 

	    } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
	        var textarea = document.createElement("textarea");
	        textarea.textContent = text;
	        textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
	        document.body.appendChild(textarea);
	        textarea.select();
	        try {
	            return document.execCommand("copy");  // Security exception may be thrown by some browsers.
	        } catch (ex) {
	            console.warn("Copy to clipboard failed.", ex);
	            return false;
	        } finally {
	            document.body.removeChild(textarea);
	        }
	    }
	}

});