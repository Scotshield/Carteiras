/**
 * Regular Expresion - Arrays
 * Esta adição ao Array.prototype irá iterar sobre o array
 * e retornará a quantidade de vezes que a expressão regular foi encontrada no array
 * Note: Esta função nao faz match sobre objetos
 * @param  {RegEx}   regx 
 * @return {Numeric} -1 significa que não foi encontrada a expressão no array nenhuma vez
 */
if (typeof Array.prototype.regexQuant === 'undefined') {
    Array.prototype.regexQuant = function (regx) {
        var count = 0;
        for (var i in this) {
            if (this[i].toString().match(regx)) {
                count++;
            }
        }
        return count == 0 ? -1 : count;
    };
}

/**
 * Regular Expresion IndexOf for Arrays
 * This little addition to the Array prototype will iterate over array
 * and return the index of the first element which matches the provided
 * regular expresion.
 * Note: This will not match on objects.
 * @param  {RegEx}   regx The regular expression to test with. E.g. /-ba/gim
 * @return {Numeric} -1 means not found
 */
if (typeof Array.prototype.reIndexOf === 'undefined') {
    Array.prototype.reIndexOf = function (regx) {
        var count = 0;
        for (var i in this) {
            if (this[i].toString().match(regx)) {
                return i;
            }
        }
        return -1;
    };
}

/**
 * Regular Expresion - Arrays
 * Esta adição ao Array.prototype irá iterar sobre o array
 * e retornará a quantidade de vezes que a expressão regular foi encontrada no array
 * e as posições (index) em que elas foram encontradas 
 * Note: Esta função nao faz match sobre objetos
 * @param  {RegEx}   regx 
 * @return {Numeric} -1 significa que não foi encontrada a expressão no array nenhuma vez
 */
if (typeof Array.prototype.regexQuantIndexOf === 'undefined') {
    Array.prototype.regexQuantIndexOf = function (regx) {
        var vetor = {};
        vetor.count = 0;
        vetor.index = [];
        for (var i in this) {
            if (this[i].toString().match(regx)) {
                vetor.index[vetor.count] = i; //guardando a posição em que a regex foi encontrada
                vetor.count++;
            }
        }
        return vetor;
    };
}