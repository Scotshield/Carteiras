//Obtendo as variáveis de configuração salvas no localStorage
var config_show_number = localStorage.getItem('config-show-number');
var config_sec_coin = localStorage.getItem('config-sec-coin');

//substituindo as variáveis no formulário
document.querySelector('#inputQuantidade').value = config_show_number;
for(var i=0; i < document.querySelector('#secCoin').options.length; i++){
  if(document.querySelector('#secCoin').options[i].value == config_sec_coin){
    document.querySelector('#secCoin').options[i].setAttribute('selected', true);
  }
}

//salvar
$('#saveConfig').on('click', function(){
    var inputQuantidadeVal = $('#inputQuantidade').val();
    var secCoinSelected = $('#secCoin option:selected').val();

    try{
      localStorage.setItem('config-show-number', inputQuantidadeVal);
      localStorage.setItem('config-sec-coin', secCoinSelected);
      $.notify("Alterações gravadas",  { position:"left top", className: "success",  autoHideDelay: 1500 });
    }catch(e){
      $.notify("Ocorreu um erro, tente novamente",  { position:"left top", className: "error",  autoHideDelay: 1500 });
    }
});
