      var coins;
      function load_data_coins(){
        $.ajax({
          type: "GET",
          url: "https://api.coinmarketcap.com/v1/ticker/?convert=" + config_sec_coin + "&limit=" + config_show_number,
          dataType: 'json',
          success: function(data){
            coin_top_1(data);
            coins_top_seg_terc(data);
            coins_top_list(data);
            show_search(); 
          },
          error: function(e){
            console.log(e);
            erro_box();
          } 
        });
      }

      function market_data(){
        $.ajax({
        type: "GET",
        url: "https://api.coinmarketcap.com/v1/global/",
        dataType: 'json',
        success: function(data){
            market_data_insert(data);
          },
        error: function(e){
            erro_market_box();
          } 
        });
      }

      function market_data_insert(data){
        var html = '' + 
          '<div class="coin-data-tile row" id="market-data">'+
            '<div class="col-12">'+
              '<table class="table market-data-general">'+
                '<tbody>'+
                  '<tr>'+
                    '<td class="info-general">' + chrome.i18n.getMessage('home_market_total_cap') + '</td>'+
                    '<td class="info-answer">' + number_transform(data.total_market_cap_usd.toString()) + '</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td class="info-general">' + chrome.i18n.getMessage('home_market_cap_24') + '</td>'+
                    '<td class="info-answer">' + number_transform(data.total_24h_volume_usd.toString()) + '</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td class="info-general">' + chrome.i18n.getMessage('home_market_active_currencies') + '</td>'+
                    '<td class="info-answer">' + number_transform(data.active_currencies.toString()) + '</td>'+
                  '</tr>'+
                  '<tr>'+
                    '<td class="info-general">' + chrome.i18n.getMessage('home_market_active_markets') + '</td>'+
                    '<td class="info-answer">' + number_transform(data.active_markets.toString()) + '</td>'+
                  '</tr>'+
                '</tbody>'+
              '</table>'+
            '</div>'+
          '</div>';

          document.querySelector("#market-data-tile").innerHTML = html;
      }

      function erro_market_box(){
        var html = '' + 
         '<div class="tile wd-full">'+
            '<div class="align-center col-8">'+
              '<span id="erro-msg" class="text-center">Erro</span>'+
            '</div>'+
         '</div>';

        document.querySelector("#market-data-tile").innerHTML = html;
      }

      function coins_top_list(coins){
        //se coins is not null
        var html = '';
        for(var i=3; i<coins.length; i++){
          var text_style = 'text-success';
          var up_down = 'up';
          if(coins[i].percent_change_1h <= 0){
            text_style = 'text-danger';
            up_down = 'down';
          }
          
          if(coins[i].name.toUpperCase() == 'ETHEREUM CLASSIC'){
            coins[i].name = 'ETH CLASSIC';
          } else if(coins[i].name.length > 12){
            coins[i].name = coins[i].name.substring(0, 12);
          }
          
          html += ''+
                  '<li class="coin-item-top-20">'+
                    '<div class="wid-47-perc display-inline-block">'+
                      '<span class="coin-item-pos">#' + (i+1) + '</span>'+
                      '<span class="coin-item-name">' + coins[i].name + '</span>'+
                    '</div>'+
                    '<div class="wid-25-perc display-inline-block">'+
                      '<span class="coin-item-val">' + parseFloat((Number(coins[i].price_usd)).toFixed(3)) + '</span><span class="coin-item-val coin-item-val-money"> USD</span>'+
                    '</div>'+
                    '<div class="wid-20-perc display-inline-block">'+
                      '<span class="coin-item-float-period">1H</span><span class="coin-item-float-perc ' + text_style + '">' + coins[i].percent_change_1h +'%</span>'+
                    '</div>'+
                    '<div class="wid-5-perc display-inline-block float-right">'+
                      '<i class="fas fa-arrow-' + up_down + ' ' + up_down +' align-middle"></i>'+
                    '</div>'+
                  '</li>';
        }
        document.querySelector("#coin-list-top-20").innerHTML = html;
      }

      function coin_top_1(coins){
        var i = 0;
        var up_down = 'up';
        var text_style = [];
        text_style[0] = 'text-success';
        text_style[1] = 'text-success';
        text_style[2] = 'text-success';
        if(coins[i].percent_change_1h <= 0){
          text_style[0] = 'text-danger';
          up_down = 'down';
        }
        if(coins[i].percent_change_24h <= 0){
          text_style[1] = 'text-danger';
        }
        if(coins[i].percent_change_7d <= 0){
          text_style[2] = 'text-danger';
        }
        var html = '' + 
           '<div class="tile wd-full">'+
              '<div class="coin-value-tile">'+
                '<span class="val-coin-tile">' + parseFloat((Number(coins[i].price_usd)).toFixed(3)) + '</span><span class="money-coin-tile"> USD</span><span class="align-middle coin-rank-tile">#' + coins[i].rank + '</span>'+
              '</div>'+
              '<div class="coin-data-tile">'+
                '<div class="row-coin-data-top">'+
                  '<span class="coin-name-val wid-93-perc">'+
                    '<span class="coin-name-tile">' + coins[i].name + '</span><span class="coin-name-tile coin-symbol-tile"> - ' + coins[i].symbol + '</span>'+
                    '<div class="coin-brl-data">'+
                      '<span class="brl-val">' + parseFloat((Number(coins[i]['price_' + config_sec_coin.toLowerCase()])).toFixed(3)) + '</span><span class="brl-money"> ' + config_sec_coin + '</span>'+
                    '</div>'+
                  '</span>'+
                  '<span class="icon-up-down ' + up_down + ' wid-7-perc align-middle">'+
                    '<i class="fas fa-arrow-' + up_down + '"></i>'+
                  '</span>'+
                '</div>'+
                '<div class="clear"></div> <!-- LIMPANDO O FLOAT -->'+
                '<div class="row">'+
                  '<div class="col-6">'+
                    '<table class="table coin-data-general">'+
                      '<tbody>'+
                        '<tr>'+
                          '<td class="info-general btc-val">BTC</td>'+
                          '<td class="info-answer btc-val">' + parseFloat((Number(coins[i].price_btc)).toFixed(9)) + '</td>'+
                        '</tr>'+
                        '<tr>'+
                          '<td class="info-general btc-val">24H - ' + config_sec_coin + '</td>'+
                          '<td class="info-answer btc-val">' + number_transform(coins[i]['24h_volume_' + config_sec_coin.toLowerCase()]) + '</td>'+
                        '</tr>'+
                        '<tr>'+
                          '<td class="info-general btc-val">MARKET - ' + config_sec_coin + '</td>'+
                          '<td class="info-answer btc-val">' + number_transform(coins[i]['market_cap_' + config_sec_coin.toLowerCase()]) + '</td>'+
                        '</tr>'+
                      '</tbody>'+
                    '</table>'+
                    '<table class="table market-coin-float">'+
                      '<thead>'+
                        '<tr>'+
                          '<th class="text-center">1h</th>'+
                          '<th class="text-center">24h</th>'+
                          '<th class="text-center">7d</th>'+
                        '</tr>'+
                      '</thead>'+
                      '<tbody>'+
                        '<tr>'+
                          '<td class="' + text_style[0] + ' text-center">'+ coins[i].percent_change_1h +' %</td>'+
                          '<td class="' + text_style[1] + ' text-center">'+ coins[i].percent_change_24h +' %</td>'+
                          '<td class="' + text_style[2] + ' text-center">'+ coins[i].percent_change_7d +' %</td>'+
                        '</tr>'+
                      '</tbody>'+
                    '</table>'+
                  '</div>'+
                  '<div class="col-6">'+
                    '<!-- chart bitcoin - COINMARKETCAP -->'+
                    '<img src="https://s2.coinmarketcap.com/generated/sparklines/web/7d/usd/1.png" class="align-center">'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>';

            document.querySelector("#front").innerHTML = html;     
      }

      function coins_top_seg_terc(coins){
        //se coins is not null
        var html = '';
        for(var i=1; i<3; i++){
          var text_style = [];
          text_style[0] = 'text-success';
          text_style[1] = 'text-success';
          text_style[2] = 'text-success';
          var up_down = 'up';
          if(coins[i].percent_change_1h <= 0){
            text_style[0] = 'text-danger';
            up_down = 'down';
          }
          if(coins[i].percent_change_24h <= 0){
            text_style[1] = 'text-danger';
          }
          if(coins[i].percent_change_7d <= 0){
            text_style[2] = 'text-danger';
          }
          html += ''+
              '<div class="tile wd-half">'+
                '<div class="coin-value-tile">'+
                  '<span class="val-coin-tile">' + parseFloat((Number(coins[i].price_usd)).toFixed(3)) + '</span><span class="money-coin-tile"> USD</span><span class="align-middle coin-rank-tile">#' + (i+1) + '</span>'+
                '</div>'+
                '<div class="coin-data-tile">'+
                  '<div class="row-coin-data-top">'+
                    '<span class="coin-name-val">'+
                      '<span class="coin-name-tile">' + coins[i].name + '</span><span class="coin-name-tile coin-symbol-tile"> - ' + coins[i].symbol + '</span>'+
                      '<div class="coin-brl-data">'+
                        '<span class="brl-val">' + parseFloat((Number(coins[i]['price_' + config_sec_coin.toLowerCase()])).toFixed(3)) + '</span><span class="brl-money"> ' + config_sec_coin + '</span>'+
                      '</div>'+
                    '</span>'+
                    '<span class="icon-up-down ' + up_down + ' align-middle">'+
                      '<i class="fas fa-arrow-'+ up_down +'"></i>'+
                    '</span>'+
                  '</div>'+
                  '<div class="clear"></div>'+
                  '<table class="table coin-data-general">'+
                    '<tbody>'+
                      '<tr>'+
                        '<td class="info-general btc-val">BTC</td>'+
                        '<td class="info-answer btc-val">' + parseFloat((Number(coins[i].price_btc)).toFixed(9)) + '</td>'+
                      '</tr>'+
                      '<tr>'+
                        '<td class="info-general btc-val">24H - ' + config_sec_coin + '</td>'+
                        '<td class="info-answer btc-val">' + number_transform(coins[i]['24h_volume_' + config_sec_coin.toLowerCase()]) + '</td>'+
                      '</tr>'+
                      '<tr>'+
                        '<td class="info-general btc-val">MARKET - ' + config_sec_coin + '</td>'+
                        '<td class="info-answer btc-val">' + number_transform(coins[i]['market_cap_' + config_sec_coin.toLowerCase()]) + '</td>'+
                      '</tr>'+
                    '</tbody>'+
                  '</table>'+
                  '<table class="table market-coin-float">'+
                    '<thead>'+
                      '<tr>'+
                        '<th class="text-center">1h</th>'+
                        '<th class="text-center">24h</th>'+
                        '<th class="text-center">7d</th>'+
                      '</tr>'+
                    '</thead>'+
                    '<tbody>'+
                      '<tr>'+
                        '<td class="' + text_style[0] + ' text-center">'+ coins[i].percent_change_1h +' %</td>'+
                        '<td class="' + text_style[1] + ' text-center">'+ coins[i].percent_change_24h +' %</td>'+
                        '<td class="' + text_style[2] + ' text-center">'+ coins[i].percent_change_7d +' %</td>'+
                      '</tr>'+
                    '</tbody>'+
                  '</table>'+
                '</div>'+
              '</div>';
        }
        document.querySelector("#tiles-sec-terc-coins").innerHTML = html;
      }

      function search_coin(coins){
        var i = 0;
        var up_down = 'up';
        var text_style = [];
        text_style[0] = 'text-success';
        text_style[1] = 'text-success';
        text_style[2] = 'text-success';
        if(coins[i].percent_change_1h <= 0){
          text_style[0] = 'text-danger';
          up_down = 'down';
        }
        if(coins[i].percent_change_24h <= 0){
          text_style[1] = 'text-danger';
        }
        if(coins[i].percent_change_7d <= 0){
          text_style[2] = 'text-danger';
        }

        console.log(coins[i]['24h_volume_' + config_sec_coin.toLowerCase()]);

        var html = '' + 
           '<div class="tile wd-full">'+
              '<div class="coin-value-tile">'+
                '<span class="val-coin-tile">' + parseFloat((Number(coins[i].price_usd)).toFixed(6)) + '</span><span class="money-coin-tile"> USD</span><span class="align-middle coin-rank-tile">#' + coins[i].rank + '</span>'+
              '</div>'+
              '<div class="coin-data-tile">'+
                '<div class="row-coin-data-top">'+
                  '<span class="coin-name-val wid-93-perc">'+
                    '<span class="coin-name-tile">' + coins[i].name + '</span><span class="coin-name-tile coin-symbol-tile"> - ' + coins[i].symbol + '</span>'+
                    '<div class="coin-brl-data">'+
                      '<span class="brl-val">' + parseFloat((Number(coins[i]['price_' + config_sec_coin.toLowerCase()])).toFixed(3)) + '</span><span class="brl-money"> ' + config_sec_coin + '</span>'+
                    '</div>'+
                  '</span>'+
                  '<span class="icon-up-down ' + up_down + ' wid-7-perc align-middle">'+
                    '<i class="fas fa-arrow-' + up_down + '"></i>'+
                  '</span>'+
                '</div>'+
                '<div class="clear"></div> <!-- LIMPANDO O FLOAT -->'+
                '<div class="container">'+
                  '<div class="row">'+
                    '<table class="table coin-data-general col-6">'+
                      '<tbody>'+
                        '<tr>'+
                          '<td class="info-general btc-val">BTC</td>'+
                          '<td class="info-answer btc-val">' + parseFloat((Number(coins[i].price_btc)).toFixed(9)) + '</td>'+
                        '</tr>'+
                        '<tr>'+
                          '<td class="info-general btc-val">24H - ' + config_sec_coin + '</td>'+
                          '<td class="info-answer btc-val">' + number_transform(coins[i]['24h_volume_' + config_sec_coin.toLowerCase()]) + '</td>'+
                        '</tr>'+
                        '<tr>'+
                          '<td class="info-general btc-val">MARKET - ' + config_sec_coin + '</td>'+
                          '<td class="info-answer btc-val">' + number_transform(coins[i]['market_cap_' + config_sec_coin.toLowerCase()]) + '</td>'+
                        '</tr>'+
                      '</tbody>'+
                    '</table>'+
                    '<table class="table market-coin-float col-6">'+
                      '<thead>'+
                        '<tr>'+
                          '<th class="text-center">1h</th>'+
                          '<th class="text-center">24h</th>'+
                          '<th class="text-center">7d</th>'+
                        '</tr>'+
                      '</thead>'+
                      '<tbody>'+
                        '<tr>'+
                          '<td class="' + text_style[0] + ' text-center">'+ coins[i].percent_change_1h +' %</td>'+
                          '<td class="' + text_style[1] + ' text-center">'+ coins[i].percent_change_24h +' %</td>'+
                          '<td class="' + text_style[2] + ' text-center">'+ coins[i].percent_change_7d +' %</td>'+
                        '</tr>'+
                      '</tbody>'+
                    '</table>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>';

            document.querySelector("#coin-search-info").innerHTML = html;        
      }

      function search_erro(){
        var msg = "";
        if(navigator.onLine){ //se tem conexão com a internet
          msg = chrome.i18n.getMessage('home_search_not_found'); //chrome.i18n

        }else{ //se não tem conexão com a internet
          msg = chrome.i18n.getMessage('home_search_no_internet'); //chrome.i18n
        }
        var html =  '<div id="search-info">'+
                      '<i class="fas fa-exclamation-triangle" style="color:#FECD07; margin-right: 5px;"></i>'+ msg +
                    '</div>';
        document.querySelector("#coin-search-info").innerHTML = html; 
      }

      function show_search(){
        $("#currencies-search").show();
      }


      function erro_box(){
        var msg = "";
        if(navigator.onLine){ //se tem conexão com a internet
          msg = chrome.i18n.getMessage('simple_error'); //chrome.i18n
        }else{ //se não tem conexão com a internet
          msg = chrome.i18n.getMessage('connection_error'); //chrome.i18n
        }

        var html = '' + 
           '<div class="tile wd-full">'+
              '<div class="align-center col-8">'+
                '<span id="erro-msg" class="text-center">' + msg + '</span>'+
              '</div>'+
           '</div>';

            document.querySelector("#front").innerHTML = html;     
      }

      //flip card onclick
      $(document).ready(function() {
          //recuperando os dados de configuração
          var config_show_number = localStorage.getItem('config-show-number');
          var config_sec_coin = localStorage.getItem('config-sec-coin');

          //carregando as informações
          load_data_coins();  
          //carregando as informações globais de mercado
          market_data();

          $(".card-flipper").click( function() {
              $(".flipper").toggleClass('flip');
          });

          $('#search-altcoin').on('change', function(){
            var coin = $('#search-altcoin').val();

            //buscando moeda digitada
            $.ajax({
              type: "GET",
              url: "https://api.coinmarketcap.com/v1/ticker/" + coin + "/?convert=" + config_sec_coin,
              dataType: 'json',
              success: function(data){
                  search_coin(data);
              },
              error: function(e){
                  console.log(e);
                  search_erro();
              } 
            });
          });

      });