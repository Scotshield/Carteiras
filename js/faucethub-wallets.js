          //inserindo os campos do formulário para salvar as coins
      var json_faucethub_wallet_coins = '[' +
                  '{"coin":"Bitcoin", "sigla":"btc"},' + 
                  '{"coin":"Ethereum", "sigla":"eth"},' + 
                  '{"coin":"Litecoin", "sigla":"ltc"},' + 
                  '{"coin":"Dogecoin", "sigla":"doge"},' + 
                  '{"coin":"Bitcoin Cash", "sigla":"bch"},' + 
                  '{"coin":"Bitcore", "sigla":"btx"},' + 
                  '{"coin":"Blackcoin", "sigla":"blk"},' + 
                  '{"coin":"Dashcoin", "sigla":"dash"},' + 
                  '{"coin":"Peercoin", "sigla":"ppc"},' + 
                  '{"coin":"Primecoin", "sigla":"xpm"},' +
                  '{"coin":"Potcoin", "sigla":"pot"}' + 
              ']';
      var exchange = 'faucethub';
      var keys_local_storage = [];

      //percorrendo as keys do localStorage e guardando em um vetor as que possuem o nome da exchange no nome
      for (var key in localStorage){
          if(key.match(/faucethub/)){
            keys_local_storage.push(key);
          }
      }

              
      //gerando os inputs automaticamente
      var cripto = JSON.parse(json_faucethub_wallet_coins);
      var html_inject = "";
      for(var i=0; i<cripto.length; i++){ //percorre cada moeda declarada no json
        var input_value   = "";
        var regex       = "";
        var key       = "";
        var cripto_vetor  = [];
        var tem_filho     = false;

        regex = new RegExp("-" + cripto[i].sigla);
        key = keys_local_storage.reIndexOf(regex);
        if(key != -1){ //se a chave existe
          input_value   = localStorage.getItem(keys_local_storage[key]);
          cripto_vetor  = keys_local_storage.regexQuantIndexOf(regex);
        }
 
        if(cripto_vetor.count < 2 || typeof cripto_vetor.count == 'undefined'){ //se tem menos de 2 keys da coin
          tem_filho = false; 
          html_inject += inputs_originais(cripto[i].coin, cripto[i].sigla, input_value, exchange, tem_filho);
        }
        else{ //se tem mais de um dado com array com a mesma moeda, estão quer dizer que ela possui inputs dinamicos
          tem_filho = true;
          html_inject += inputs_originais(cripto[i].coin, cripto[i].sigla, input_value, exchange, tem_filho);

          for(var x=0; x<cripto_vetor.index.length; x++){
            key = keys_local_storage[cripto_vetor.index[x]];
            
            if(typeof key != 'undefined'){
              if(key.match(/\d+/)){ //se na key for encontrado número
                var num_rand = keys_local_storage[cripto_vetor.index[x]].match(/\d+/)[0];
                input_value = localStorage.getItem(key);
                html_inject += inputs_dinamicos(cripto[i].coin, cripto[i].sigla, num_rand, input_value, exchange);
              }
            }             
          }
          html_inject += '</div>'; //fechando a div do input original (como ele tem filhos, a div não foi fechada no começo)
        }
        
      } 
      //inserindo os inputs no formulário
      document.querySelector("#form-wallet").innerHTML = html_inject; 

      /*
       *
       */
      function inputs_originais(nm_coin, sigla_coin, input_value, nm_exchange, tem_filho){
        var html = ''+
          '<div class="form-group input_fields_wrap">' + 
               '<div class="input-group">' + 
                  '<span class="input-group-addon">' + 
                      '<img src="img/faucethub-' + sigla_coin + '.png" class="wid-30">' + 
                  '</span>' + 
                  '<input class="form-control form-control-sm" type="text" data-type="faucethub-coin" data-coin="' + sigla_coin + '" data-coin-name="' + nm_coin + '" id="' + exchange + '-' + sigla_coin + '" placeholder="Faucethub - ' + nm_coin + '" value="' + input_value + '">' + 
                '<button class="btn btn-info btn-sm add_field_button" data-coin="' + sigla_coin + '" data-coin-name="' + nm_coin + '" id="add_field_' + nm_exchange + '_' + sigla_coin + '" data-refer-id=' + nm_exchange + '-' + sigla_coin + '">+</button>' +
                '</div>';
            if(!tem_filho){ //se não tem filho, fecha a div.
              html += '</div>';
              //caso ela tenha inputs dinamicos filho, então a div deverá ser fechada no escopo que chama a função inputs_originais()
            } 
            return html;
      }

      /*
       *
       */
      function inputs_dinamicos(nm_coin, sigla_coin, num_rand, input_value, nm_exchange){
        return '<div class="input-group">'+
              '<span class="input-group-addon">'+
              '<div class="wid-30"></div>'+
              '</span>'+
              '<input class="form-control form-control-sm" data-type="faucethub-coin" data-coin="' + sigla_coin + '" data-coin-name="' + nm_coin + '"  type="text" id="' +  nm_exchange + '-' + sigla_coin + '_' + num_rand + '" placeholder="Faucethub - ' + nm_coin + '" value="' + input_value + '">'+
              '<button class="btn btn-danger btn-sm remove_field" id="remove_field_' + sigla_coin + '" data-refer-id="'+  nm_exchange + '-' + sigla_coin + '_' + num_rand + '">-</button>'+
            '</div>';
      }

      //quando o documento carregar, executa
      $(document).ready(function() { 
          var wrapper           = $(".input_fields_wrap"); //Fields wrapper
          var add_button        = $(".add_field_button"); //Add button 'add'

        /* FUNÇÃO
         * Adiciona um input quando o botão de add for clicado
         */
          $(add_button).click(function(e){ 
              e.preventDefault();
              var input_value   = "";
              var coin_sigla    = $(this).attr("data-coin"); //referencia da moeda (coin) - sigla
              var coin_name     = $(this).attr("data-coin-name"); //nome da moeda
              var num_rnd     = getRandomInt(1, 1000); //gerando var (int) random entre 1 e 1000.
                wrapper       = $(this).parent('div').parent('.input_fields_wrap'); //local onde o HTML será injetado
                
                $(wrapper).append(inputs_dinamicos(coin_name, coin_sigla, num_rnd, '',  exchange));
          });


        /* FUNÇÃO
         * Remove o input que teve o botão de remover clicado
         */
          $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
              e.preventDefault(); 
              try{
                var local_storage_key = $(this).attr("data-refer-id"); //obtendo a key do localStorage
                localStorage.removeItem(local_storage_key); //removendo a carteira do localStorage
                $(this).parent('div').remove(); //removendo a div com a carteira (retirando do HTML)
                flashMsg("warning", chrome.i18n.getMessage('delete_success_msg')); //chrome.i18n - Campo removido com sucesso
              }catch(e){
                console.log(err.message);
              }
          })


        /* FUNÇÃO
         * Salva no localStorage o valor do input com data-type="coin" quando ocorre alguma alteração 
         */
        $("#form-wallet").on("change", "[data-type='faucethub-coin']", function(){
          var sigla_coin = $(this).attr("id");
          try {
            localStorage.setItem(sigla_coin,$(this).val()); 
            flashMsg("success", chrome.i18n.getMessage('alteration_success')); //chrome.i18n  - Alteração efetuada com sucesso          
          }
          catch(e) {
              console.log(err.message); 
              flashMsg("warning", chrome.i18n.getMessage('save_error_msg')); //chrome.i18n - Ocorreu um erro ao salvar. Tente novamente
          }
        });

        /* FUNÇÃO
         * Função que gera um número inteiro randômico dentro de um intervalo 
         */
        function getRandomInt(min, max) {
          min = Math.ceil(min);
          max = Math.floor(max);
          return Math.floor(Math.random() * (max - min)) + min;
        }

        function flashMsg(status, msg){
          $("#flash_msg").addClass('alert-' + status);
          $("#flash_msg").html(msg);
          $("#flash_msg").fadeIn("slow");
          setInterval(function(){
            $("#flash_msg").fadeOut("slow", function(){
              $("#flash_msg").removeClass('alert-' + status);           
            });
          },3000);
        }

      });