/* VERIFICA SE EXISTEM VARIÁVEIS DE CONFIGURAÇÃO */

var config_show_number = localStorage.getItem('config-show-number');
var config_sec_coin = localStorage.getItem('config-sec-coin');
var valid_coins = ['EUR', 'BRL', 'RUB', 'ARG'];


if(config_show_number == null || config_show_number == ""){
  localStorage.setItem('config-show-number', 15); //15 é o valor default
} else if(config_show_number < 5 || config_show_number > 30){
  localStorage.setItem('config-show-number', 15); //15 é o valor default
}

if(config_sec_coin == null || config_sec_coin == ""){
  localStorage.setItem('config-sec-coin', 'EUR'); //EUR (euro) é o valor default
} else if(!find_in_array(valid_coins, config_sec_coin)) { //se a moeda salva não for encontrada no array de moedas válidas, então troca a moeda salva para EURO
  localStorage.setItem('config-sec-coin', 'EUR'); //EUR (euro) é o valor default  
}

function find_in_array(array, value) {
  if(array.indexOf(value) > -1) {
    return true; //encontrou
  } else {
    return false; //não encontrou
  }
}
