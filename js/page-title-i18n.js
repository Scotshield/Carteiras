//dependencia - chrome.i18n

function insert_page_title(part_1, part_2){
	var html = '' + 
		'<h1 class="text-center">'+
			'<span class="cinza">' + part_1 + '</span><span class="dourado">' + part_2 + '</span>'+
		'</h1>';

	document.querySelector('#page-title').innerHTML = html;
}

var title = chrome.i18n.getMessage(document.querySelector("#page-title").getAttribute("data-i18n-page-title"));
var part_1 = title.substring(0, ((title.length)/1.7).toFixed(0));
var part_2 = title.substring(((title.length)/1.7).toFixed(0), title.length);
insert_page_title(part_1, part_2);