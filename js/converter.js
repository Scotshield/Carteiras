var coins;
var indice_orig = null
var indice_dest = null;
var amount = 1;

//recuperando as variáveis de configuração
var config_show_number = localStorage.getItem('config-show-number');
var config_sec_coin = localStorage.getItem('config-sec-coin');

$.ajax({
  type: "GET",
  url: "https://api.coinmarketcap.com/v1/ticker/?convert=" + config_sec_coin + "&limit=2000",
  dataType: 'json',
  success: function(data){
    remove_loading();
    $('#convert-altcoin').show();
    load_options(data);
  },
  error: function(e){
    erro_box();
  } 
});

//remove a tela de loading
function remove_loading(){
  $('#loading').remove();
}

//Carrega as opções de moedas e demais funções para cálculo de conversão
function load_options(coins){

  coins[coins.length] = load_national_currencies(coins, 'Dolar', 'USD');

  //configurando a moeda secundária - a conversão só é possível entre altcoins, dolar e uma moeda secundária
  if(config_sec_coin == 'BRL'){
    coins[coins.length] = load_national_currencies(coins, 'Real Brazil', 'BRL');
  }else if(config_sec_coin == 'EUR'){
    coins[coins.length] = load_national_currencies(coins, 'Euro', 'EUR');
  }else if(config_sec_coin == 'RUB'){
    coins[coins.length] = load_national_currencies(coins, 'Rublo', 'RUB');
  }else if(config_sec_coin == 'ARG'){
    coins[coins.length] = load_national_currencies(coins, 'Peso Argentino', 'ARG');
  }

  var html = "";
  var i=0;
  for(i=0; i<coins.length; i++){
    html += '<option value="' + coins[i].name + '" data-indice="' + i + '">' + coins[i].symbol + '</option>';
  }

  document.querySelector("#altcoins-datalist-orig").innerHTML = html; 
  document.querySelector("#altcoins-datalist-dest").innerHTML = html; 

  //quando ocorrer alteração na quantidade de moedas
  $("#amount").on('change', function(e){
    if($(this).val() <= 0){
      document.querySelector("#flash-erro-msg-orig").innerHTML = chrome.i18n.getMessage('converter_flash_error_amount');//chrome.i18n
      $(this).val(0.1);
      amount = Big(0.1);
    }else{
      amount = Big($(this).val());
      document.querySelector("#flash-erro-msg-orig").innerHTML = "";
    }
    calcula_conversao(coins, amount);
  });

  //quando ocorrer alteração na moeda de origem
  $("input[name='altcoins-datalist-orig']").on('change', function(e){
    var val = $(this).val().toUpperCase();
    var list = $(this).attr('list');
      var indice = $('#'+list + ' option').filter(function() { //procura a moeda digitada no datalist e caso exista, retorna o seu data-indice
        return ($(this).val().toUpperCase() === val);
      }).data('indice');

      if(typeof indice != 'undefined') {
        //moeda está na lista
        indice_orig = indice;
        document.querySelector("#flash-erro-msg-orig").innerHTML = ""; //limpa a mensagem de "moeda não encontrada"
        calcula_conversao(coins, amount);
      } else {
        //moeda não está na lista
        indice_orig = null;
        document.querySelector("#flash-erro-msg-orig").innerHTML = chrome.i18n.getMessage('converter_flash_error_not_found'); //chrome.i18n
      }
    });

  //quando ocorrer alteração na moeda de destino
  $("input[name='altcoins-datalist-dest']").on('change', function(e){
    var val = $(this).val().toUpperCase();
    var list = $(this).attr('list');
      var indice = $('#'+list + ' option').filter(function() { //procura a moeda digitada no datalist e caso exista, retorna o seu data-indice
        return ($(this).val().toUpperCase() === val);
      }).data('indice');

      if(typeof indice != 'undefined') {
        // valor está na lista
        indice_dest = indice;
        document.querySelector("#flash-erro-msg-dest").innerHTML = ""; //limpa a mensagem de "moeda não encontrada"
        calcula_conversao(coins, amount);
      } else {
        indice_dest = null;
        document.querySelector("#flash-erro-msg-dest").innerHTML = chrome.i18n.getMessage('converter_flash_error_not_found'); //chrome.i18n
      }
    });
}

//função que verifica que um valor está ou não no Array
function inArray(array, value){
  if(array.indexOf(value) != -1){
    return true;
  }
  return false;
}

//Efetua o calculo da conversão de uma moeda para outra
function calcula_conversao(coins, amount){
  if(amount <= 0){
    amount = 1;
  }
  if((indice_orig != null)&&(indice_dest != null)){
    var array_national_coins = ["USD", config_sec_coin]; //["USD", "BRL"];
    var resultado = "";
    var national_coin = true; //variável para verificar se há conversão de altcoin para moeda nacional

    if((inArray(array_national_coins, coins[indice_orig].symbol)) && (inArray(array_national_coins, coins[indice_dest].symbol))){ //transforma real <-> dolar
      if(coins[indice_orig].symbol == coins[indice_dest].symbol){
        resultado = 1*amount;       
      }else if(coins[indice_orig].symbol == config_sec_coin){ //}else if(coins[indice_orig].symbol == 'BRL'){ 
        resultado = (coins[0].price_usd/coins[0]['price_' + config_sec_coin.toLowerCase()])*amount; //coins[0] = btc
      }else{
        resultado = (coins[0]['price_' + config_sec_coin.toLowerCase()]/coins[0].price_usd)*amount;
      }

    }else if(inArray(array_national_coins, coins[indice_orig].symbol)){
      resultado = (1 * amount)/coins[indice_dest]['price_' + coins[indice_orig].symbol.toLowerCase()];
      national_coin = false;
    
    }else if(inArray(array_national_coins, coins[indice_dest].symbol)){
      resultado = coins[indice_orig]['price_' + coins[indice_dest].symbol.toLowerCase()] * amount;
    
    }else{
      var coin_orig = Big(coins[indice_orig].price_btc);
      var coin_dest = Big(coins[indice_dest].price_btc);
      resultado = Big(coin_orig/coin_dest)*amount;
      national_coin = false;
    }

    //configurando a precisão da conversão
    var conv = 0;
    var isBig = false;
    if(!national_coin){
      conv = resultado.toFixed(12);
      isBig = true; 
    } else{
      conv = resultado.toFixed(2);
      isBig = false;
    }

    document.querySelector("#coins_converted").innerHTML = '<span id="converted_show">' + (isBig ? '<span class="dark">' + conv.substring(0,(conv.length-4)) + '</span>' + '<span class="light">' + conv.substring((conv.length-4)) + '</span>' : resultado.toFixed(2)) + ' ' + coins[indice_dest].symbol + '</div>';      
  }else{
    document.querySelector("#coins_converted").innerHTML = '';
  }
}

var coin = [];

function load_national_currencies(coins, coin_name, coin_sigla){
    var json = ''+
                '{'+
                  '"id": "' + coin_name + '", '+
                  '"name": "' + coin_name + '", '+
                  '"symbol": "' + coin_sigla + '", '+
                  '"rank": "national_currency", '+
                  '"price_usd": "1", '+
                  '"price_btc": "1", '+
                  '"24h_volume_usd": "1", '+
                  '"market_cap_usd": "1", '+
                  '"available_supply": "1", '+
                  '"total_supply": "1", '+
                  '"max_supply": "1", '+
                  '"percent_change_1h": "1", '+
                  '"percent_change_24h": "1", '+
                  '"percent_change_7d": "1", '+
                  '"last_updated": "1", '+
                  '"price_' + coin_sigla.toLowerCase() + '": "1", '+
                  '"24h_volume_' + coin_sigla.toLowerCase() + '": "1", '+
                  '"market_cap_' + coin_sigla.toLowerCase() + '": "1"'+
                '}';
  return(JSON.parse(json));
}

//Apresenta uma mensagem de erro para quando houver problemas na conexão
function erro_box(){
  var msg = "";
  if(navigator.onLine){ //se tem conexão com a internet
    msg = chrome.i18n.getMessage('simple_error'); //chrome.i18n
  }else{ //se não tem conexão com a internet
    msg =  chrome.i18n.getMessage('connection_error'); //chrome.i18n
  }
  var html = '' + 
  '<div id="erro_box">'+
    '<div class="align-vert-center">'+
      '<i class="display-block fas fa-exclamation-triangle text-center" style="color:#FECD07; font-size:1.4em; margin-bottom: 5px;"></i>'+
      '<span id="erro-msg" class="display-block text-center">' + msg + '</span>'+
    '</div>'+
  '</div>';

  document.querySelector("#container").innerHTML = html;     
}
